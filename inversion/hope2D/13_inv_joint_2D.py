#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: rrochlitz
"""

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.invmesh_tools import PrismWorld
from custEM.meshgen import meshgen_utils as mu
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import pygimli as pg
from saem import CSEMData
from custEM.inv.inv_utils import coverage
from pygimli.frameworks import JointModelling

# ########################################################################### #
# 2d joint inversion #


def topo_f(x, y):
	return(x*0. + 590.) #590.

invmod1 = 'SP9joint_l10f08_large'
invmesh1 = 'invmesh2d_' + invmod1
invmod2 = 'dummy9_joint_l10f08_large'
invmesh2 = 'invmesh2d_' + invmod2

dataname1 = 'SP9norotBxByBz'
dataname0 = 'SP9rotBxByBz'
saemdata1 = np.load(dataname1+".npz", allow_pickle=True)
saemdata0 = np.load(dataname0+".npz", allow_pickle=True)
print([freq for freq in saemdata1["freqs"]], len(saemdata1["freqs"]))
dataname2 = 'MP9Bz'
saemdata2 = np.load(dataname2+".npz", allow_pickle=True)
print([freq for freq in saemdata2["freqs"]], len(saemdata2["freqs"]))

aErr = 2e-3
sig_bg = 1e-2

# %% calculate response for data2 of final inversion results for misfit plots

fop1 = MultiFWD(invmod1, invmesh1, saem_data=saemdata1, inv_type='domains',
                sig_bg=sig_bg, p_fwd=1, n_cores=72, start_iter=0)

fop2 = MultiFWD(invmod2, invmesh2, saem_data=saemdata2, inv_type='domains',
                sig_bg=sig_bg, p_fwd=1, n_cores=72, start_iter=0)#

invmodel = np.load(fop1.inv_dir + 'sig_iter_8.npy')
fop2.response(invmodel)
np.save(fop2.inv_dir + 'response_iter_99.npy', fop2.last_response)

raise SystemExit


txs = saemdata0['tx']
for ti in range(4):
    txs[ti][1, 0] = txs[ti][0, 0]

txs = [mu.refine_path(txs[ti], length=20.) for ti
       in range(4)]

yoff = 1600.
for tx in txs:
    tx[:,1] -= yoff

inkl = 90. - 64.95
#dekl = -81.28
dekl = -13.28

bothrx = [data["rx"] for data in saemdata0["DATA"]] +\
         [data["rx"] for data in saemdata2["DATA"]]

# %% 2D mesh generation

P = PrismWorld(name=invmesh1,                 # mesh name
                x_extent=[-4000., 4000.],      # extent in profile direction
                x_reduction=600.,             # reduced extent at bottom
                y_depth=4000.,                 # prism length in +- y direction
                z_depth=2000.,                 # depth of inversion area
                n_prisms=260,                  # number of prisms at surface
                txs=txs,         # transmitters at surface
                orthogonal_txs=[True] * 4,
                prism_area=1e5,               # maximum x-z area of prisms
                prism_quality=34.,            # quality of 2D inversion mesh
                x_dim=[-2e4, 2e4],            # x-extent of inner 3D mesh
                y_dim=[-2e4, 2e4],            # y-extent of inner 3D mesh
                z_dim=[-2e4, 2e4],            # z-extent of inner 3D mesh
                topo=topo_f,                  # 2D topography function
                )

# add receiver locations to parameter file for all receiver patches

allrx = mu.resolve_rx_overlaps(bothrx, 2.)
allrx[:, 1] -= yoff

rx_tri = mu.refine_rx(allrx, 2., 30.)
P.PrismWorld.add_paths(rx_tri)

for rx in [data["rx"] for data in saemdata0["DATA"]]:
    rx[:, 1] -= yoff
    P.PrismWorld.add_rx(rx)

for i in range(len(P.PrismWorld.txs)):
    P.PrismWorld.txs[i][:, 2] = P.PrismWorld.get_topo_vals(P.PrismWorld.txs[i])
    P.PrismWorld.txs[i] = mu.rotate_around_point(P.PrismWorld.txs[i],
                                                  [0., 0., 0.],
                                                  [np.deg2rad(112.)],
                                                  ['z'])

for i in range(len(P.PrismWorld.rxs)):
 	P.PrismWorld.rxs[i] = mu.rotate_around_point(P.PrismWorld.rxs[i],
                                                  [0., 0., 0.],
                                                  [np.deg2rad(112.)],
                                                  ['z'])

P.PrismWorld.post_rotate = [112.]
P.PrismWorld.call_tetgen(tet_param='-pDq1.3aA', print_infos=False)


# %% 2D mesh generation

P = PrismWorld(name=invmesh2,                 # mesh name
                x_extent=[-4000., 4000.],      # extent in profile direction
                x_reduction=600.,             # reduced extent at bottom
                y_depth=4000.,                 # prism length in +- y direction
                z_depth=2000.,                 # depth of inversion area
                n_prisms=260,                  # number of prisms at surface
                txs=txs,         # transmitters at surface
                orthogonal_txs=[True] * 4,
                prism_area=1e5,               # maximum x-z area of prisms
                prism_quality=34.,            # quality of 2D inversion mesh
                x_dim=[-2e4, 2e4],            # x-extent of inner 3D mesh
                y_dim=[-2e4, 2e4],            # y-extent of inner 3D mesh
                z_dim=[-2e4, 2e4],            # z-extent of inner 3D mesh
                topo=topo_f,                  # 2D topography function
                )

# add receiver locations to parameter file for all receiver patches

allrx = mu.resolve_rx_overlaps(bothrx, 2.)
allrx[:, 1] -= yoff

rx_tri = mu.refine_rx(allrx, 2., 30.)
P.PrismWorld.add_paths(rx_tri)

for rx in [data["rx"] for data in saemdata2["DATA"]]:
    rx[:, 1] -= yoff
    P.PrismWorld.add_rx(rx)

    # # build refined triangles around receivers in the mesh
    # rx_tri = mu.refine_rx(rx, 1., 30.)
    # P.PrismWorld.add_paths(rx_tri)

for i in range(len(P.PrismWorld.txs)):
    P.PrismWorld.txs[i][:, 2] = P.PrismWorld.get_topo_vals(P.PrismWorld.txs[i])
    tmp = mu.rotate_around_point(P.PrismWorld.txs[i],
                                      [0., 0., 0.],
                                      [np.deg2rad(112.)],
                                      ['z'])
    tmp = mu.rotate_around_point(tmp,
                                  [0., 0., 0.],
                                  [np.deg2rad(inkl)],
                                  ['x'])

    P.PrismWorld.txs[i] = mu.rotate_around_point(tmp,
                                      [0., 0., 0.],
                                      [np.deg2rad(dekl)],
                                      ['z'])

for i in range(len(P.PrismWorld.txs)):
 	tmp = mu.rotate_around_point(P.PrismWorld.rxs[i],
                                       [0., 0., 0.],
                                       [np.deg2rad(112.)],
                                       ['z'])

 	tmp = mu.rotate_around_point(tmp,
                                  [0., 0., 0.],
                                  [np.deg2rad(inkl)],
                                  ['x'])
 	P.PrismWorld.rxs[i] = mu.rotate_around_point(tmp,
                                      [0., 0., 0.],
                                      [np.deg2rad(dekl)],
                                      ['z'])

P.PrismWorld.post_rotate = [112., inkl, dekl]
P.PrismWorld.topo = None

P.PrismWorld.call_tetgen(tet_param='-pDq1.3aA', print_infos=False, ignore_tx_z=True)

###############################################################################

# %% set up forward operators
pfwd = 1
fop1 = MultiFWD(invmod1, invmesh1, saem_data=saemdata1, inv_type='domains',
                sig_bg=sig_bg, p_fwd=pfwd, n_cores=64, start_iter=0)

fop2 = MultiFWD(invmod2, invmesh2, saem_data=saemdata2, inv_type='domains',
                sig_bg=sig_bg, p_fwd=pfwd, n_cores=64, start_iter=0)

dataVec = np.concatenate((fop1.measured, fop2.measured))
errorVec = np.concatenate((fop1.errors, fop2.errors))

pgmesh = pg.load('meshes/mesh_create/' + invmesh1 + '.bms')

fopJoint = JointModelling([fop1, fop2])
fopJoint.setMesh(pgmesh)
fopJoint.setRegionProperties("*", limits=[1e-4, 1e0])
fopJoint.setData([pg.Vector(fop1.measured),
                  pg.Vector(fop2.measured)])

inv = pg.Inversion(verbose=True)
inv.setForwardOperator(fopJoint)
inv.setPostStep(fop1.analyze)
# run inversion
invmodel = inv.run(dataVec, errorVec,  lam=20., lamFactor=0.8, verbose=True,
                  startModel=sig_bg, maxIter=21)

# %% save results
np.save(fop1.inv_dir + 'final_inv_model.npy', invmodel)
# invmodel = np.load(fop1.inv_dir + 'final_inv_model.npy')

for n in range(2):
    inv.fop.fops[n].response(invmodel)
    inv.fop.fops[n].import_jacobian()
    inv.fop.fops[n]._jac.resize(*inv.fop.fops[n].J.shape)
    for i, row in enumerate(inv.fop.fops[n].J):
        inv.fop.fops[n]._jac.setRow(i, row)

pgmesh = fop1.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel
pgmesh['coverage'] = coverage(inv, invmodel)
pgmesh.exportVTK(fop1.inv_dir + invmod1 + '_final_invmodel.vtk')