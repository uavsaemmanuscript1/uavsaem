# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 16:08:18 2021

@author: Rochlitz.R
"""
import matplotlib
matplotlib.use('pdf')

import numpy as np
import os
from saem import CSEMSurvey
from saem.tools import readCoordsFromKML

if not os.path.isdir('../../data/hope2D'):
    import zipfile
    with zipfile.ZipFile("../../data/hope2D.zip", "r") as zfile:
        zfile.extractall("../../data")

if not os.path.isdir('data'):
    os.makedirs('data')
if not os.path.isdir('plots'):
    os.makedirs('plots')

# %% import Mat files

orig = [528000, -2606000., 0.]
ddir = "../../data/hope2D/"

self = CSEMSurvey()
for ti in [0, 1, 2, 3]:
    new = True
    if os.path.isdir(ddir + "Tx" + str(ti) + "/MA"):
        if new:
            self.addPatch(ddir + "Tx" + str(ti) + "/MA/SS*.mat",
                          name=str(ti))
            new = False
        else:
            self[-1].addData(ddir + "Tx" + str(ti) + "/MA/SS*.mat")

    tx = readCoordsFromKML(ddir + "Tx" + str(ti) + ".kml", zone=33)
    self[-1].ry -= 1e7
    self[-1].tx, self[-1].ty, self[-1].tz = tx[:, ::tx.shape[1]-1]
    self[-1].ERR.real = np.abs(self[-1].ERR.real)
    self[-1].ERR.imag = np.abs(self[-1].ERR.imag)

# set new local coordinate system origin
self.setOrigin(orig)
# rotate survey so that lines match N-S direction for detection
self.rotate(np.deg2rad(22.))
for pi, patch in enumerate(self.patches):
    # first survey in the west
    if pi < 4:
        patch.detectLines(np.linspace(-2700., -500., 12), axis='x')

#%%

# delete highest frequencies
self.filter(fmax=150)
# delete every second point to decrease amount of data for inversion
self.filter(every=2)
# remove everything around transmitters
self.filter(minTxDist=280.)

for pi, patch in enumerate(self.patches):

    patch.ERR.real = np.abs(patch.ERR.real)
    patch.ERR.imag = np.abs(patch.ERR.imag)
    patch.estimateError(relError=0.05, absError=2e-3)
    patch.deactivateNoisyData(rErr=1.)
    patch.RESP = np.zeros_like(patch.DATA)

    # below, some manual data selection (delete artifact points) happens
    # for each patch
    if pi == 0:
        patch.DATA[:, :, 0] = np.nan + 1j * np.nan
        patch.DATA[:, :, 1] = np.nan + 1j * np.nan
        patch.DATA[:, :, 20] = np.nan + 1j * np.nan

    if pi == 1:
        patch.DATA[:, :2, np.logical_and(
            patch.ry < 400., patch.ry > -700.)] =\
                np.nan + 1j * np.nan

    if pi == 3:
        patch.DATA[:, :, 169:175] = np.nan + 1j * np.nan
        patch.DATA[:, :, 144:148] = np.nan + 1j * np.nan
        patch.DATA[:, :, 45] = np.nan + 1j * np.nan
        patch.DATA[:, :, 236] = np.nan + 1j * np.nan
        patch.DATA[:, :, 255] = np.nan + 1j * np.nan
        patch.DATA[:, :, 258] = np.nan + 1j * np.nan
        patch.DATA[:, :, 94] = np.nan + 1j * np.nan
        patch.DATA[:, :, 230:233] = np.nan + 1j * np.nan
        patch.DATA[:, :, 202:208] = np.nan + 1j * np.nan

    # here we can plot our data linewise or patchwise
    # patch.generateDataPDF('plots/MagArrow_data_Tx_' + str(pi) + '.pdf',
    #                       mode='linefreqwise', axis='y')

# rotate data and coordinates back to original orientation
self.rotate(np.deg2rad(-22.))

# export data files
for li in [3, 6, 9, 12]:
    self.basename='data/MA'
    self.saveData(cmp=[0, 0, 1], line=li)

# export rotated coordinates for simplified prism mesh generation
self.rotate(np.deg2rad(22. + 90.))
for li in [3, 6, 9, 12]:
    self.basename='data/MArot'
    self.saveData(cmp=[0, 0, 1], line=li)