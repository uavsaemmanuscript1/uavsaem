#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Rochlitz.R
"""

from custEM.meshgen.invmesh_tools import PrismWorld
from custEM.meshgen import meshgen_utils as mu
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import pygimli as pg


def topo_f(x, y):
    return(x * 0. + 590.)

# ########################################################################## #
# SHFT data 2D inversion

# specfiy profile line, either 3, 6, 9 or 12
line = 9
invmod = 'SHFT_line' + str(line)
invmesh = 'invmesh2d_' + invmod
# specify background resistivity
sig_bg = 1e-2

# import data, for mesh generation purposes also a rotated version
dataname = 'data/SHFT-line' + str(line) + 'BxByBz.npz'
rotname = 'data/SHFTrot-line' + str(line) + 'BxByBz.npz'
saemdata = np.load(dataname, allow_pickle=True)
rotdata = np.load(rotname, allow_pickle=True)

# homogenize Tx x-coordinates to have an exact 2D Tx geometry
txs = rotdata['tx']
for ti in range(4):
    txs[ti][:, 0] = np.mean([txs[ti][0, 0], txs[ti][1, 0]])


txs = [mu.refine_path(txs[ti], length=20.) for ti
       in range(4)]

# additional shift to center prism mesh around y = 0
yoff = 1600.
for tx in txs:
    tx[:, 1] -= yoff

# %% 2D mesh generation

P = PrismWorld(name=invmesh,                 # mesh name
               x_extent=[-3000., 3000.],     # extent in profile direction
               x_reduction=500.,             # reduced extent at bottom
               y_depth=2000.,                # prism length in +- y direction
               z_depth=1200.,                # depth of inversion area
               n_prisms=200,                 # number of prisms at surface
               txs=txs,                      # transmitters at surface
               orthogonal_txs=[True] * 4,
               prism_area=1e5,               # maximum x-z area of prisms
               prism_quality=34.,            # quality of 2D inversion mesh
               x_dim=[-1e4, 1e4],            # x-extent of inner 3D mesh
               y_dim=[-1e4, 1e4],            # y-extent of inner 3D mesh
               z_dim=[-1e4, 1e4],            # z-extent of inner 3D mesh
               topo=topo_f,                  # 2D topography function
               )

# avoid building intersecting refinement triangles
allrx = mu.resolve_rx_overlaps([data["rx"] for data in rotdata["DATA"]], 2.)
allrx[:, 1] -= yoff

# build refined triangles around receivers in the mesh
rx_tri = mu.refine_rx(allrx, 2., 30.)
P.PrismWorld.add_paths(rx_tri)

# add receiver locations to parameter file for all receiver patches
for rx in [data["rx"] for data in rotdata["DATA"]]:
    rx[:, 1] -= yoff
    P.PrismWorld.add_rx(rx)

for i in range(len(P.PrismWorld.txs)):
    P.PrismWorld.txs[i][:, 2] = P.PrismWorld.get_topo_vals(P.PrismWorld.txs[i])
    P.PrismWorld.txs[i] = mu.rotate_around_point(P.PrismWorld.txs[i],
                                                 [0., 0., 0.],
                                                 [np.deg2rad(112.)],
                                                 ['z'])

for i in range(len(P.PrismWorld.rxs)):
 	P.PrismWorld.rxs[i] = mu.rotate_around_point(P.PrismWorld.rxs[i],
                                                 [0., 0., 0.],
                                                 [np.deg2rad(112.)],
                                                 ['z'])

# ensure that the complete mesh is rotate before running the inversion back
# to the real orientation of the survey to match correct horizontal field
# directions
P.PrismWorld.post_rotate = [112.]

# build 2.5D prism mesh
P.PrismWorld.call_tetgen(tet_param='-pDq1.3aA', print_infos=False)

###############################################################################
# %% run inversion

# set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=saemdata, sig_bg=sig_bg,
               n_cores=60, p_fwd=1, start_iter=0, inv_type='domains')
fop.setRegionProperties("*", limits=[1e-4, 1])

# set up inversion operator
inv = pg.Inversion()
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)

# run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=10., lamFactor=0.8,
                   verbose=True, startModel=fop.sig_0, maxIter=21)

###############################################################################
# %% post-processingelf
np.save(fop.inv_dir + 'inv_model.npy', invmodel)
pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1. / invmodel


# cov = np.zeros(fop._jac.cols())
# for i in range(fop._jac.rows()):
#     cov += np.abs(fop._jac.row(i))
# cov /= pgmesh.cellSizes()
# np.save(fop.inv_dir + invmod + '_coverage.npy', cov)
# pgmesh['coverage'] = cov
pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')