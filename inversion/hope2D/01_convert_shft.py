# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 16:08:18 2021

@author: Rochlitz.R
"""

import matplotlib
matplotlib.use('pdf')

import numpy as np
import os
from saem import CSEMSurvey
from saem.tools import readCoordsFromKML

if not os.path.isdir('../../data/hope2D'):
    import zipfile
    with zipfile.ZipFile("../../data/hope2D.zip", "r") as zfile:
        zfile.extractall("../../data")

if not os.path.isdir('data'):
    os.makedirs('data')
if not os.path.isdir('plots'):
    os.makedirs('plots')

# %% convert Mat files

self = CSEMSurvey()
ddir = "../../data/hope2D/"

for ti in [0, 1, 2, 3]:
    new = True
    if os.path.isdir(ddir + "Tx" + str(ti)):
        if new:
            self.addPatch(ddir + "Tx" + str(ti) + "/SS*.mat",
                          name=str(ti))

    self[-1].ry -= 1e7
    tx = readCoordsFromKML(ddir + "Tx" + str(ti) + ".kml", zone=33)
    self[-1].tx, self[-1].ty, self[-1].tz = tx[:, ::tx.shape[1] - 1]


# shift to local coordinate frame
orig = [528000, -2606000.]
self.setOrigin(orig)

# rotate that lines/Tx align with East/North directions for
# line detection
self.rotate(np.deg2rad(22.))

# sort data by applying indices for lines
for pi, patch in enumerate(self.patches):
    patch.detectLines(np.linspace(-2700., -500., 12), axis='x')

# filter out data in vicinity of Tx (240m) and above 1024 H
self.filter(fmax=1060)
self.filter(minTxDist=240.)

for pi, patch in enumerate(self.patches):
    # delete data points identified as artifacts
    if pi in [2, 3]:
        patch.filter(minTxDist=320.)
    if pi == 0:
        # 32 and 96 Hz for all cmp
        patch.DATA[:, :2, :] = np.nan + 1j * np.nan

    if pi == 1:
        # 32 Hz for Bx/By
        patch.DATA[:2, :1, :] = np.nan + 1j * np.nan

    if pi == 2:
        # 32 Hz for Bx/By
        patch.DATA[:2, :1, :] = np.nan + 1j * np.nan
        #for ll in [1, 3, 6, 8, 11, 13]:
        for ll in [2, 4]:
            patch.DATA[:, :, patch.line==ll] = np.nan + 1j * np.nan
        patch.DATA[:, :, np.isclose(patch.ry, 1000., atol=600.)] =\
                np.nan + 1j * np.nan
        patch.DATA[2, :, np.isclose(patch.ry, 2000., atol=1000.)] =\
                np.nan + 1j * np.nan
    if pi == 3:
        # 32 and 96 Hz for all cmp
        patch.DATA[:, :2, :] = np.nan + 1j * np.nan
        patch.DATA[:, :, np.logical_and(
            patch.line==3, np.isclose(patch.ry, 850., atol=50.))] =\
                np.nan + 1j * np.nan
        patch.DATA[:, :, np.logical_and(
            patch.line==4, np.isclose(patch.ry, 100., atol=50.))] =\
                np.nan + 1j * np.nan
        patch.DATA[:, :, np.logical_and(
            np.logical_and(patch.line > 5, patch.line < 9), patch.ry < 200.)] =\
                np.nan + 1j * np.nan
        patch.DATA[2, -1, patch.line > 9] = np.nan + 1j * np.nan
        patch.DATA[2, -2:, patch.line == 6] = np.nan + 1j * np.nan
        patch.DATA[:, :, 204:208] = np.nan + 1j * np.nan
        patch.DATA[:, :, 150:156] = np.nan + 1j * np.nan
    

# rotate back to original geografic system
self.rotate(np.deg2rad(-22.))
# flip Tx direction to match with Data 
self.revertTx()

for pi, patch in enumerate(self.patches):
    patch.estimateError(relError=0.05, absError=2e-3)
    patch.deactivateNoisyData(rErr=1.)
    patch.RESP = np.zeros_like(patch.DATA)
    # plot data linewise or patchwise
    # patch.generateDataPDF('plots/SHFT_data_Tx_' + str(pi) + '.pdf',
    #                       mode='linefreqwise', axis='y')

# export data files
for li in [3, 6, 9, 12]:
    self.basename='data/SHFT'
    self.saveData(cmp=[1, 1, 1], line=li)
    
# export rotated coordinates for simplified prism mesh generation
self.rotate(np.deg2rad(22. + 90.))
for li in [3, 6, 9, 12]:
    self.basename='data/SHFTrot'
    self.saveData(cmp=[1, 1, 1], line=li)
    