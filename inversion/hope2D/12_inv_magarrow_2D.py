#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: rrochlitz
"""

from custEM.meshgen.meshgen_tools import BlankWorld
from custEM.meshgen.invmesh_tools import PrismWorld
from custEM.meshgen import meshgen_utils as mu
from custEM.inv.inv_utils import MultiFWD
import numpy as np
import pygimli as pg
from saem import CSEMData
from saem.tools import coverage


def topo_f(x, y):
    return(x * 0. + 590.)


# ########################################################################## #
# MagArrow data 2D inversion

# specfiy profile line, either 3, 6, 9 or 12
line = 6
invmod = 'MA_line' + str(line)
invmesh = 'invmesh2d_' + invmod
# specify background resistivity
sig_bg = 1e-2

dataname = 'data/MA-line' + str(line) + 'Bz.npz'
saemdata = np.load(dataname, allow_pickle=True)

inkl = 90. - 64.95
dekl = -13.28

txs = saemdata['tx']
for ti in range(4):
    txs[ti][1, 0] = txs[ti][0, 0]

txs = [mu.refine_path(txs[ti], length=20.) for ti
       in range(4)]

yoff = 1600.
for tx in txs:
    tx[:,1] -= yoff

# %% 2D mesh generation

P = PrismWorld(name=invmesh,                 # mesh name
                x_extent=[-4000., 4000.],      # extent in profile direction
                x_reduction=600.,             # reduced extent at bottom
                y_depth=4000.,                 # prism length in +- y direction
                z_depth=2000.,                 # depth of inversion area
                n_prisms=260,                  # number of prisms at surface
                txs=txs,         # transmitters at surface
                orthogonal_txs=[True] * 4,
                prism_area=1e5,               # maximum x-z area of prisms
                prism_quality=34.,            # quality of 2D inversion mesh
                x_dim=[-2e4, 2e4],            # x-extent of inner 3D mesh
                y_dim=[-2e4, 2e4],            # y-extent of inner 3D mesh
                z_dim=[-2e4, 2e4],            # z-extent of inner 3D mesh
                topo=topo_f,                  # 2D topography function
                )

# avoid building intersecting refinement triangles
allrx = mu.resolve_rx_overlaps([data["rx"] for data in saemdata["DATA"]], 2.)
allrx[:, 1] -= yoff

# build refined triangles around receivers in the mesh
rx_tri = mu.refine_rx(allrx, 2., 30.)
P.PrismWorld.add_paths(rx_tri)

# add receiver locations to parameter file for all receiver patches
for rx in [data["rx"] for data in saemdata["DATA"]]:
    rx[:, 1] -= yoff
    P.PrismWorld.add_rx(rx)

for i in range(len(P.PrismWorld.txs)):
    P.PrismWorld.txs[i][:, 2] = P.PrismWorld.get_topo_vals(P.PrismWorld.txs[i])

    tmp = mu.rotate_around_point(P.PrismWorld.txs[i],
                                 [0., 0., 0.],
                                 [np.deg2rad(112.)],
                                 ['z'])

    tmp = mu.rotate_around_point(tmp,
                                 [0., 0., 0.],
                                 [np.deg2rad(dekl)],
                                 ['z'])

    P.PrismWorld.txs[i] = mu.rotate_around_point(tmp,
                                                 [0., 0., 0.],
                                                 [np.deg2rad(inkl)],
                                                 ['x'])

for i in range(len(P.PrismWorld.txs)):
 	tmp = mu.rotate_around_point(P.PrismWorld.rxs[i],
                                       [0., 0., 0.],
                                       [np.deg2rad(112.)],
                                       ['z'])

 	tmp = mu.rotate_around_point(tmp,
                                  [0., 0., 0.],
                                  [np.deg2rad(dekl)],
                                  ['z'])
 	P.PrismWorld.rxs[i] = mu.rotate_around_point(tmp,
                                      [0., 0., 0.],
                                      [np.deg2rad(inkl)],
                                      ['x'])

P.PrismWorld.post_rotate = [112., dekl, inkl]
P.PrismWorld.topo = None

P.PrismWorld.call_tetgen(tet_param='-pDq1.3aA',
                         print_infos=False, ignore_tx_z=True)

# mesh = pg.Mesh('./meshes/mesh_create/invmesh2d_MP3.1.vtk')
# torot = mesh.positions().array()
# torot = mu.rotate_around_point(torot,
#                                 [0., 0., 0.],
#                                 [-np.deg2rad(112)],
#                                 ['z'])

# for jj in range(mesh.nodeCount()):
#     mesh.node(jj).setPos(torot[jj])

# mesh.exportVTK('./meshes/mesh_create/invmesh2d_MP3.1.vtk')
# asd

###############################################################################
# %% run inversion

# set up forward operator
fop = MultiFWD(invmod, invmesh, saem_data=saemdata, sig_bg=sig_bg,
                n_cores=64, p_fwd=1, start_iter=0, inv_type='domains')
fop.setRegionProperties("*", limits=[1e-4, 1])
# fop.import_jacobian()
# asd

# set up inversion operator
inv = pg.Inversion()
inv.setForwardOperator(fop)
inv.setPostStep(fop.analyze)

# run inversion
invmodel = inv.run(fop.measured, fop.errors, lam=10., lamFactor=0.8,
                   verbose=True, startModel=fop.sig_0, maxIter=21)

###############################################################################

# %% post-processing and rotation back to original coordinate system
np.save(fop.inv_dir + 'inv_model.npy', invmodel)

pgmesh = fop.mesh()
pgmesh['sigma'] = invmodel
pgmesh['res'] = 1./invmodel
pgmesh['coverage'] = coverage(inv)

torot = pgmesh.positions().array()

torot = mu.rotate_around_point(torot,
                               [0., 0., 0.],
                               [np.deg2rad(-dekl)],
                               ['z'])
torot = mu.rotate_around_point(torot,
                               [0., 0., 0.],
                               [np.deg2rad(-inkl)],
                               ['x'])

for jj in range(pgmesh.nodeCount()):
    pgmesh.node(jj).setPos(torot[jj])

pgmesh.exportVTK(fop.inv_dir + invmod + '_final_invmodel.vtk')
