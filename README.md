# UAVSAEM

### Supplementary data repository for Hope and Poderosa 3D inversion results 

## Further updates will follow shortly after functionality of scripts with current code version has been checked 

- files for reproducing the results are available in the inversion directory
- python scripts are compatible with custEM version 1.6 (Dec. 2024), pygimli 1.5, and SAEM 
- transfer functions are stored as zip files in the data directory and are unzipped on-the-fly  
- data, misfit and model plots are available in the corresponding directories